FROM docker:23.0.1-git

ARG KUBECTL_VERSION=1.30.2
ARG HELM_VERSION=3.17.0

ENV HOME=/config

RUN apk add --no-cache curl ca-certificates git bash gettext

RUN wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x /usr/local/bin/kubectl && \
    kubectl version --client

RUN set -x && \
    wget -O /tmp/helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -xzvf /tmp/helm.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    rm -rf linux-amd64 /tmp/helm.tar.gz && \
    chmod +x /usr/local/bin/helm && \
    helm version

# Create non-root user (with a randomly chosen UID/GUI).
RUN adduser deployer -Du 2342 -h /config

USER deployer